#!/bin/bash

REV=$1
if [ $# -eq 0 ]; then
  REV=demo
fi

ssh eight45 "mkdir tmp/mass-springs/$REV"
scp dist/bundle.js dist/index.html eight45:~/tmp/mass-springs/$REV/

echo "http://kira.solar/pub/mass-springs/$REV/"
