#!/bin/bash

set -e

echo "Compiling.."
tsc

echo "Bundling.."
browserify dist/program.js > dist/bundle.js
