export class Vec2 {
  x: number
  y: number

  constructor(x: number, y: number) {
    this.x = x
    this.y = y
  }

  static random(): Vec2 {
    return new Vec2(Math.random()*2-1, Math.random()*2-1)
  }

  set(x, y: number) {
    this.x = x
    this.y = y
  }

  add(a: Vec2): Vec2 {
    this.x += a.x
    this.y += a.y
    return this
  }

  sub(a: Vec2): Vec2 {
    this.x -= a.x
    this.y -= a.y
    return this
  }

  scale(c: number): Vec2 {
    this.x *= c
    this.y *= c
    return this
  }

  length(): number {
    return Math.sqrt(this.x*this.x + this.y*this.y)
  }

  distanceTo(a: Vec2): number {
    const dx = a.x - this.x
    const dy = a.y - this.y
    return Math.sqrt(dx*dx + dy*dy)
  }

  clone(): Vec2 {
    return new Vec2(this.x, this.y)
  }

  normalize(): Vec2 {
    const len = this.length()
    if (len === 0) return this.clone()
    return this.clone().scale(1.0/this.length())
  }
}

