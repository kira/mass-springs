import { Vec2 } from './vec2'
const pixelbutler = require('pixelbutler')

const RED = {r: 255, g: 0, b: 0}
const GREEN = {r: 0, g: 255, b: 0}

export class PointMass {
  mass: number
  pos: Vec2
  vel: Vec2
  netForce: Vec2

  constructor(mass: number, x: number, y: number) {
    this.mass = mass
    this.pos = new Vec2(x, y)
    this.vel = new Vec2(0, 0)
    this.netForce = new Vec2(0, 0)
  }

  applyForce(f: Vec2) {
    this.netForce.add(f)
  }

  update() {
    this.netForce.scale(1.0/this.mass)
    this.vel.add(this.netForce)
    this.netForce.set(0, 0)
    this.vel.x *= 0.85
    this.vel.y *= 0.85
    this.pos.add(this.vel)
  }

  render(pb: any) {
    pb.fillCircle(this.pos.x, this.pos.y, this.mass*2, RED)
  }
}

export class Spring {
  stiffness: number
  restingDistance: number
  mass1: PointMass
  mass2: PointMass

  constructor(stiffness, rd: number, m1, m2: PointMass) {
    this.stiffness = stiffness
    this.restingDistance = rd
    this.mass1 = m1
    this.mass2 = m2
  }

  update() {
    const dist = this.mass1.pos.distanceTo(this.mass2.pos)
    const fMag = this.stiffness * (dist - this.restingDistance)

    const force1 = this.mass2.pos
      .clone()
      .sub(this.mass1.pos)
      .normalize()
      .scale(fMag)
    this.mass1.applyForce(force1)

    const force2 = this.mass1.pos
      .clone()
      .sub(this.mass2.pos)
      .normalize()
      .scale(fMag)
    this.mass2.applyForce(force2)
  }

  render(pb: any) {
    let x = this.mass1.pos.x
    let y = this.mass1.pos.y
    let dx = this.mass2.pos.x - x
    let dy = this.mass2.pos.y - y
    const dist = Math.sqrt(dx*dx + dy*dy)
    dx /= dist
    dy /= dist
    x += dx
    y += dy

    const stepSize = dist / this.restingDistance

    for (let i=0; i < dist/stepSize; i++) {
      pb.setPixel(x, y, GREEN)
      x += dx * stepSize
      y += dy * stepSize
    }
  }
}
