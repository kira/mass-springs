const shell = require('game-shell')()
const pixelbutler = require('pixelbutler')
import { PointMass, Spring } from './lib/mass-springs'
import { Vec2 } from './lib/vec2'

let masses: PointMass[] = []
let springs: Spring[] = []
let pb
let tix=0
let paused = false

function makeCell(x, y, radius: number) {
  const centre = new PointMass(1.0, x, y)
  const steps = 7
  const stepSizeRadians = Math.PI * 2 / steps
  let prev, first
  let rot = 0
  for(let i=0; i < steps; i++) {
    const mass = new PointMass(1.0,
      x + Math.cos(rot) * radius/4,
      y + Math.sin(rot) * radius/4)
    addMass(mass)

    // Connect perimeter point + centre
    addSpring(new Spring(0.06, radius, mass, centre))

    // Connect previous perimeter point to this one
    if (prev) {
      addSpring(new Spring(0.02, 5.0, prev, mass))
    } else {
      first = mass
    }
    prev = mass

    rot += stepSizeRadians
  }

  // Connect first and last
  addSpring(new Spring(0.02, 5.0, prev, first))

  for (const m of masses) {
    m.applyForce(new Vec2(
      Math.random() * 2 - 1,
      Math.random() * 2 - 1))
  }
}

makeCell(64, 64, 32)

function addSpring(s: Spring) {
  springs.push(s)
}
function addMass(m: PointMass) {
  masses.push(m)
}

shell.preventDefaults = false
shell.bind('toggle-pause', 'P')

shell.once('init', () => {
  pb = new pixelbutler.Stage({
      width: 160,
      height: 120,
      canvas: 'game'
  })
})

var red = {r: 255, g: 0, b: 0}
var green = {r: 0, g: 255, b: 0}
var blue = {r: 0, g: 0, b: 255}
var black = {r: 0, g: 0, b: 0}
var white = {r: 255, g: 255, b: 255}

let pausedDown = false

shell.on('tick', () => {
  if (!pausedDown && shell.wasDown('toggle-pause')) {
    pausedDown = true
    paused = !paused
  }
  if (shell.wasUp('toggle-pause')) {
    pausedDown = false
  }
  if (paused) return

  tix++

  for (const m of masses) {
    m.update()
  }
  for (const s of springs) {
    s.update()
  }

  if (tix % 3 === 0) {
    const m = masses[Math.floor(Math.random()*masses.length)]
    m.applyForce(Vec2.random().scale(Math.random()*2.0))
  }
})

shell.on('render', () => {
  if (paused) return

  pb.clear(black)

  for (const m of masses) {
    m.render(pb)
  }
  for (const s of springs) {
    s.render(pb)
  }

  pb.render()
})

